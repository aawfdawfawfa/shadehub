
-- Gui to Lua
-- Version: 3.2

-- Instances:

local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("ImageLabel")
local Keyhere = Instance.new("TextBox")
local ShadeHubText = Instance.new("TextLabel")
local Login_Roundify_8px = Instance.new("ImageLabel")
local Login = Instance.new("TextButton")

--Properties:

ScreenGui.Parent = game.CoreGui

Frame.Name = "Frame"
Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(3, 167, 255)
Frame.BackgroundTransparency = 1.000
Frame.Position = UDim2.new(0.403790087, 0, 0.256756753, 0)
Frame.Size = UDim2.new(0, 366, 0, 218)
Frame.Image = "rbxassetid://3570695787"
Frame.ImageColor3 = Color3.fromRGB(3, 167, 255)
Frame.ScaleType = Enum.ScaleType.Slice
Frame.SliceCenter = Rect.new(100, 100, 100, 100)
Frame.SliceScale = 0.667

Keyhere.Name = "Keyhere"
Keyhere.Parent = Frame
Keyhere.BackgroundColor3 = Color3.fromRGB(0, 85, 255)
Keyhere.Position = UDim2.new(0.226775959, 0, 0.38532111, 0)
Keyhere.Size = UDim2.new(0, 200, 0, 50)
Keyhere.Font = Enum.Font.SourceSans
Keyhere.Text = "KeyHere"
Keyhere.TextColor3 = Color3.fromRGB(0, 0, 0)
Keyhere.TextSize = 14.000

ShadeHubText.Name = "ShadeHubText"
ShadeHubText.Parent = Frame
ShadeHubText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ShadeHubText.BackgroundTransparency = 1.000
ShadeHubText.BorderColor3 = Color3.fromRGB(27, 42, 53)
ShadeHubText.Position = UDim2.new(0.226775959, 0, 0.0642201826, 0)
ShadeHubText.Size = UDim2.new(0, 200, 0, 50)
ShadeHubText.Font = Enum.Font.SourceSans
ShadeHubText.Text = "ShadeHub Prime"
ShadeHubText.TextColor3 = Color3.fromRGB(0, 85, 255)
ShadeHubText.TextScaled = true
ShadeHubText.TextSize = 14.000
ShadeHubText.TextWrapped = true

Login_Roundify_8px.Name = "Login_Roundify_8px"
Login_Roundify_8px.Parent = Frame
Login_Roundify_8px.Active = true
Login_Roundify_8px.AnchorPoint = Vector2.new(0.5, 0.5)
Login_Roundify_8px.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Login_Roundify_8px.BackgroundTransparency = 1.000
Login_Roundify_8px.Position = UDim2.new(0.5, 0, 0.800458729, 0)
Login_Roundify_8px.Selectable = true
Login_Roundify_8px.Size = UDim2.new(0.546448112, 0, 0.233944952, 0)
Login_Roundify_8px.Image = "rbxassetid://3570695787"
Login_Roundify_8px.ImageColor3 = Color3.fromRGB(0, 85, 255)
Login_Roundify_8px.ScaleType = Enum.ScaleType.Slice
Login_Roundify_8px.SliceCenter = Rect.new(100, 100, 100, 100)
Login_Roundify_8px.SliceScale = 0.667

Login.Name = "Login"
Login.Parent = Login_Roundify_8px
Login.BackgroundColor3 = Color3.fromRGB(0, 85, 255)
Login.BackgroundTransparency = 1.000
Login.BorderColor3 = Color3.fromRGB(27, 42, 53)
Login.BorderSizePixel = 0
Login.Position = UDim2.new(-0.00363385677, 0, 0.0685374737, 0)
Login.Size = UDim2.new(0, 200, 0, 44)
Login.Font = Enum.Font.SourceSans
Login.Text = "Login"
Login.TextColor3 = Color3.fromRGB(0, 0, 0)
Login.TextSize = 14.000
